﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace DiceForger.Core.Data {
    public class DiceForgerDbContextFactory : IDesignTimeDbContextFactory<DiceForgerDbContext> {
        public DiceForgerDbContext CreateDbContext(string[] args) {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();

            DbContextOptionsBuilder<DiceForgerDbContext> dbContextBuilder = new ();

            string connectionString = configuration.GetConnectionString("DefaultConnection");

            dbContextBuilder.UseSqlServer(connectionString);

            return new DiceForgerDbContext(dbContextBuilder.Options);
        }
    }
}
