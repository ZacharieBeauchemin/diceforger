﻿using System.Threading.Tasks;
using Microsoft.JSInterop;

namespace DiceForger.WebApp.Data;

/// <summary>
/// Access the preference from the user stored in IndexDb.
/// </summary>
public class PreferenceStore : LocalStore {
    private const string _store = "Preferences";

    public PreferenceStore(IJSRuntime js) : base(js) {
    }
    /// <summary>
    /// Check if the setup was marked as completed.
    /// </summary>
    /// <returns>A boolean representing if the setup was completed.</returns>
    public async ValueTask<bool> IsSetupCompleted()
        => await GetAsync<bool>(_store, "SetupCompleted");

    /// <summary>
    /// Mark the setup as completed.
    /// </summary>
    /// <returns>An async Task.</returns>
    public async Task MarkSetupAsCompleted()
        => await PutAsync(_store, "SetupCompleted", true);

    /// <summary>
    /// Get the language for the user.
    /// </summary>
    /// <returns>The language that the user saved.</returns>
    public async ValueTask<Language> GetLanguage()
        => await GetAsync<Language>(_store, "Language");

    /// <summary>
    /// Set the language for the user.
    /// </summary>
    /// <param name="language">The language selected by the user.</param>
    /// <returns>An async Task.</returns>
    public async Task SetLanguage(Language language)
        => await PutAsync(_store, "Language", (int)language);

    /// <summary>
    /// Get the theme for the user.
    /// </summary>
    /// <returns>The theme that the user saved.</returns>
    public async ValueTask<Theme> GetTheme()
        => await GetAsync<Theme>(_store, "Theme");

    /// <summary>
    /// Set the theme for the user.
    /// </summary>
    /// <param name="theme">The theme selected by the user.</param>
    /// <returns>An async Task.</returns>
    public async Task SetTheme(Theme theme)
        => await PutAsync(_store, "Theme", (int)theme);

    /// <summary>
    /// Check if the user wants to include the rebellion extension.
    /// </summary>
    /// <returns>A boolean representing if the extension should be used.</returns>
    public async ValueTask<bool> GetIncludeRebellion()
        => await GetAsync<bool>(_store, "IncludeRebellion");

    /// <summary>
    /// Set if the user wants to include the rebellion extension.
    /// </summary>
    /// <param name="includeRebellion">A boolean representing if the user wants to include rebellion.</param>
    /// <returns>An async Task.</returns>
    public async Task SetIncludeRebellion(bool includeRebellion)
        => await PutAsync(_store, "IncludeRebellion", includeRebellion);
}