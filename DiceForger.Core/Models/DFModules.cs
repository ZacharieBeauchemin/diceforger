﻿using System.Collections.Generic;

namespace DiceForger.Core.Models;

public class DFModules {
    public int Id { get; set; }
    public string Name { get; set; }
    public virtual IEnumerable<DFCard> RequiredCards { get; set; }
}
