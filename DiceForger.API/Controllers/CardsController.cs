﻿using DiceForger.Core.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using DiceForger.Core.Repositories;

namespace DiceForger.API.Controllers;


[Route("[controller]")]
[ApiController]
public class CardsController : ControllerBase {
    private IDFCardsRepository cardsRepository;

    public CardsController(IDFCardsRepository cardsRepository) {
        this.cardsRepository = cardsRepository;
    }

    [HttpGet]
    [Route("GetAll")]
    public async Task<IEnumerable<DFCard>> GetAll() {
        return await cardsRepository.GetCardsAsync();
    }
}