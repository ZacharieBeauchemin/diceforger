﻿namespace DiceForger.WebApp.Data;

public enum Language {
    English = 0,
    French  = 1
}

public enum Theme {
    Dark  = 0,
    Light = 1
}