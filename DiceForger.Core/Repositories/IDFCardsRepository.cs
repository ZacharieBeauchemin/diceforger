﻿using DiceForger.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DiceForger.Core.Repositories;

public interface IDFCardsRepository {
    Task<bool> AddCardAsync(DFCard card);
    Task<DFCard> GetCardAsync(int id);
    Task<IEnumerable<DFCard>> GetCardsAsync();
    IEnumerable<DFCard> QueryCard(Func<DFCard, bool> predicate);
    Task<bool> RemoveCardAsync(int id);
    Task<bool> UpdateCardAsync(DFCard card);
}