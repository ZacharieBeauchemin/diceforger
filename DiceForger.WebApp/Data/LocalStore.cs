﻿using System.Threading.Tasks;
using Microsoft.JSInterop;

namespace DiceForger.WebApp.Data;

/// <summary>
/// An abstract class to interact with IndexDb
/// </summary>
public abstract class LocalStore {
    private readonly IJSRuntime js;

    protected LocalStore(IJSRuntime js) {
        this.js = js;
    }

    /// <summary>
    /// Get an element from IndexDb.
    /// </summary>
    /// <typeparam name="T">The Item to get.</typeparam>
    /// <param name="storeName">The name of the store in IndexDb.</param>
    /// <param name="key">The key of the item to get.</param>
    /// <returns>An async ValueTask with the item fetched from IndexDb.</returns>
    protected ValueTask<T> GetAsync<T>(string storeName, object key)
        => js.InvokeAsync<T>("localStore.get", storeName, key);

    /// <summary>
    /// Get all elements from IndexDb.
    /// </summary>
    /// <typeparam name="T">The Item to get.</typeparam>
    /// <param name="storeName">The name of the store in IndexDb.</param>
    /// <returns>An async ValueTask with the items fetched from IndexDb.</returns>
    protected ValueTask<T[]> GetAllAsync<T>(string storeName)
        => js.InvokeAsync<T[]>("localStore.getAll", storeName);

    /// <summary>
    /// Put an element in IndexDb.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="storeName">The name of the store in IndexDb.</param>
    /// <param name="key">The key of the item to put.</param>
    /// <param name="value">The item to put.</param>
    /// <returns>An async ValueTask.</returns>
    protected ValueTask PutAsync<T>(string storeName, object key, T value)
        => js.InvokeVoidAsync("localStore.put", storeName, key, value);

    /// <summary>
    /// Delete an element in IndexDb.
    /// </summary>
    /// <param name="storeName">The name of the store in IndexDb.</param>
    /// <param name="key">The key of the item to put.</param>
    /// <returns>An async ValueTask.</returns>
    protected ValueTask DeleteAsync(string storeName, object key)
        => js.InvokeVoidAsync("localStore.delete", storeName, key);
}
