﻿using DiceForger.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace DiceForger.Core.Data;

#nullable disable
public class DiceForgerDbContext : IdentityDbContext<IdentityUser> {
    public DiceForgerDbContext(DbContextOptions<DiceForgerDbContext> options) : base(options) {

    }

    public DbSet<DFCard> DFCards { get; set; }
}