﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiceForger.API.Models;
using DiceForger.Core.Data;

namespace LePapetier_Api.Controllers;

[Route("[controller]")]
[ApiController]
public class AuthController : ControllerBase {
    private readonly DiceForgerDbContext context;
    private readonly UserManager<IdentityUser> userManager;
    private readonly SignInManager<IdentityUser> signInManager;
    private readonly RoleManager<IdentityRole> roleManager;

    public AuthController(DiceForgerDbContext context, UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager, SignInManager<IdentityUser> signInManager) {
        this.context = context;
        this.userManager = userManager;
        this.roleManager = roleManager;
        this.signInManager = signInManager;
    }

    [Route("token")]
    [HttpPost]
    public async Task<IActionResult> CreateToken(string email, string password) {
        if (await IsValidEmailAndPassword(email, password)) {
            return new ObjectResult(await GenerateToken(email));
        } else {
            return BadRequest();
        }
    }

    [Route("register")]
    [HttpPost]
    public async Task<bool> Register(NewUserModel newUser) {
        if (ModelState.IsValid) {
            IdentityUser user = new() { UserName = newUser.Username, Email = newUser.Email };
            IdentityResult userCreationResult = await userManager.CreateAsync(user, newUser.Password);

            return userCreationResult.Succeeded;
        } else {
            return false;
        }
    }

    [NonAction]
    private async Task<bool> IsValidEmailAndPassword(string email, string password) {
        IdentityUser? user = await userManager.FindByEmailAsync(email);
        return await userManager.CheckPasswordAsync(user, password);
    }

    [NonAction]
    private async Task<dynamic> GenerateToken(string email) {
        IdentityUser? user = await userManager.FindByEmailAsync(email);

        var roles = from ur in context.UserRoles join r in context.Roles on ur.RoleId equals r.Id where ur.UserId == user.Id select new { ur.UserId, ur.RoleId, r.Name };

        var claims = new List<Claim> {
            new Claim(ClaimTypes.Name, email),
            new Claim(ClaimTypes.NameIdentifier, user.Id),
            new Claim(JwtRegisteredClaimNames.Nbf, new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds().ToString()),
            new Claim(JwtRegisteredClaimNames.Exp, new DateTimeOffset(DateTime.Now.AddDays(1)).ToUnixTimeSeconds().ToString())
        };

        foreach (var role in roles) {
            claims.Add(new Claim(ClaimTypes.Role, role.Name));
        }

        JwtSecurityToken? token = new JwtSecurityToken(
            new JwtHeader(
                new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes("z839$8rpum4A6*QXcx6%Q$Nc$Ccz7e")),
                    SecurityAlgorithms.HmacSha256)),
            new JwtPayload(claims)
        );

        var output = new {
            Access_Token = new JwtSecurityTokenHandler().WriteToken(token),
            UserName = email
        };

        return output;
    }
}