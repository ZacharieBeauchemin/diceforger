﻿(function () {
    const diceForgerDb = idb.openDB('DiceForgerDB', 1, {
        blocked: () => {
            // seems an older version of this app is running in another tab
            console.log(`Please close this app opened in other browser tabs.`);
        },
        upgrade(db, oldVersion, newVersion, transaction) {
            if (oldVersion === 0) upgradeFromV0ToV1();

            function upgradeFromV0ToV1() {
                db.createObjectStore("DFCards", { keyPath: "id" });
                db.createObjectStore("Preferences");
                
                const preferences = transaction.objectStore("Preferences");
                preferences.put(false, "SetupCompleted");
                preferences.put(0, "Language");
                preferences.put(0, "Theme");
                preferences.put(true, "IncludeRebellion");
            }
        },
        blocking: () => {
            // seems the user just opened this app again in a new tab
            // which happens to have gotten a version change
            console.log(`DiceForger is outdated, please close this tab`);
        }
    });
    
    window.localStore = {
        get: async (storeName, key) => (await diceForgerDb).transaction(storeName).store.get(key),
        getAll: async (storeName) => (await diceForgerDb).transaction(storeName).store.getAll(),
        put: async (storeName, key, value) => (await diceForgerDb).transaction(storeName, "readwrite").store.put(value, key === null ? undefined : key),
        delete: async (storeName, key) => (await diceForgerDb).transaction(storeName, "readwrite").store.delete(key)
    };
})();