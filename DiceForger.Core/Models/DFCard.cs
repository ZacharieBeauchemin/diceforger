﻿using System.Collections.Generic;

namespace DiceForger.Core.Models;

public class DFCard {
    public int Id { get; set; }
    public string Name { get; set; }

    public int GloryPoints { get; set; }

    public List<DFCardCost> Costs { get; set; }
    public List<DFCardEffect> Effects { get; set; }

    public CardSet Set { get; set; }

    public bool ModuleSpecific { get; set; }

    public override string ToString() => Name;

    public enum CardSet {
        Standard,
        Alternative,
        Goddess,
        Titans
    }
}