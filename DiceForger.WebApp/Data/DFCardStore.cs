﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DiceForger.Core.Models;
using Microsoft.JSInterop;

namespace DiceForger.WebApp.Data;

/// <summary>
/// Access the dice forge cards stored in IndexDb.
/// </summary>
public class DFCardStore: LocalStore {
    private const string _store = "DFCards";

    public DFCardStore(IJSRuntime js) : base(js) {
    }

    /// <summary>
    /// Get an array of all cards from IndexDb.
    /// </summary>
    /// <returns>Every card.</returns>
    public ValueTask<DFCard[]> GetAll() {
        return GetAllAsync<DFCard>(_store);
    }

    /// <summary>
    /// Put a card in the IndexDb.
    /// </summary>
    /// <param name="card">The card to put.</param>
    /// <returns>An async Task.</returns>
    public async Task Put(DFCard card)
        => await PutAsync(_store, null, card);

    /// <summary>
    /// Put a list of card in the IndexDb.
    /// </summary>
    /// <param name="cards">An IEnumerable of cards.</param>
    /// <returns>An async Task.</returns>
    public async Task PutMultiple(IEnumerable<DFCard> cards) {
        foreach (DFCard card in cards) {
                await Put(card);
        }
    }

    /// <summary>
    /// Delete a card in the IndexDb.
    /// </summary>
    /// <param name="id">The id of the card to delete.</param>
    /// <returns>An async Task.</returns>
    public async Task Delete(int id)
        => await DeleteAsync(_store, id);
}
