﻿using DiceForger.Core.Models;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

using static Microsoft.EntityFrameworkCore.EntityState;
using DiceForger.Core.Data;

namespace DiceForger.Core.Repositories;

public class DFCardsRepository : IDFCardsRepository {
    private readonly DiceForgerDbContext databaseContext;

    public DFCardsRepository(DiceForgerDbContext dbContext) {
        databaseContext = dbContext;
    }

    public async Task<bool> AddCardAsync(DFCard card) {
        try {
            EntityEntry<DFCard> tracking = await databaseContext.DFCards.AddAsync(card);

            await databaseContext.SaveChangesAsync();

            return tracking.State == Added;

        } catch (Exception) {
            return false;
        }
    }

    public async Task<DFCard> GetCardAsync(int id) {
        try {
            DFCard card = await databaseContext.DFCards.Include(c => c.Costs).Include(c => c.Effects).FirstOrDefaultAsync(c => c.Id == id);
            return card;
        } catch (Exception) {
            return null;
        }
    }

    public async Task<IEnumerable<DFCard>> GetCardsAsync() {
        try {
            IEnumerable<DFCard> cards = await databaseContext.DFCards.Include(c => c.Costs).Include(c => c.Effects).ToListAsync();
            return cards;
        } catch (Exception) {
            return new List<DFCard>();
        }
    }

    public IEnumerable<DFCard> QueryCard(Func<DFCard, bool> predicate) {
        try {
            IEnumerable<DFCard> cards = databaseContext.DFCards.Where(predicate);
            return cards;
        } catch (Exception) {
            return new List<DFCard>();
        }
    }

    public async Task<bool> RemoveCardAsync(int id) {
        try {
            DFCard card = await databaseContext.DFCards.FindAsync(id);

            if (card == null) return false;

            EntityEntry<DFCard> tracking = databaseContext.DFCards.Remove(card);

            await databaseContext.SaveChangesAsync();

            return tracking.State == Deleted;

        } catch (Exception) {
            return false;
        }
    }

    public async Task<bool> UpdateCardAsync(DFCard card) {
        try {
            EntityEntry<DFCard> tracking = databaseContext.DFCards.Update(card);

            await databaseContext.SaveChangesAsync();

            return tracking.State == Modified;

        } catch (Exception) {
            return false;
        }
    }
}
